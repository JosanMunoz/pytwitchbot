import socket
import select
import time
import os
import Components
from Components import *
from configparser import ConfigParser

class TwitchBot:
    def __init__(self):
        
        self.cfg = ConfigParser()
        self.cfg.read('config.ini')
        self.Components = []
        self.sock = None
        self.Active = False
        self.Error = False

        #Create Components by string        
        CompList = self.cfg.get("User", "ComponentList").split(",")
        CompList = [Comp.strip() for Comp in CompList]
        for component in CompList:
            self.Components.append(eval(component)(self.cfg))

    def Connect(self):
        self.sock = None
        waitTime = 5
        while waitTime < 30:
            for res in socket.getaddrinfo(self.cfg.get('Socket','HOST'),
                                          self.cfg.getint('Socket','PORT'),
                                          socket.AF_UNSPEC,
                                          socket.SOCK_STREAM):
                af, socktype, proto, canonname, sa = res
                try:
                    self.sock = socket.socket(af, socktype, proto)
                    self.sock.connect(sa)
                    break
                except OSError as msg:
                    print("Oserror: "+ str(msg))
                    self.sock = None
                    continue

            if self.sock is None:
                print('Could not open socket, waiting ' + str(waitTime) + ' seconds before retrying')
                waitTime += waitTime
                time.sleep(waitTime)
            else:
                break
        if self.sock is None:
            print('Could not open socket, closing')
            sys.exit(1)

        self.Active = True
        UserName = self.cfg.get("User", "UserName")
        self.sock.send("PASS {}\r\n".format(self.cfg.get("User","Pass")).encode("utf-8"))
        self.sock.send("NICK {}\r\n".format(UserName).encode("utf-8"))
        self.sock.send("JOIN #{}\r\n".format(UserName).encode("utf-8"))

    def SendMsg(self, msg):
        #print("PRIVMSG #{0} :{1}\r\n".format(Username,msg))
        self.sock.send("PRIVMSG #{0} :{1}\r\n".format(self.cfg.get("User","UserName"),msg).encode("utf-8"))

    def Start(self):
        UserName = self.cfg.get("User","UserName")
        while self.Active:
            read, write, err = \
                    select.select([self.sock],
                                  [self.sock],
                                  [])
            if read:
                try:
                    recieve = self.sock.recv(2048).decode("utf-8")
                except socket.timeout:
                    print("Socket Timed out on recv")
                    continue
                except:
                    print("Unexpected error at: self.sock.recv()")
                    self.Error = True
                    self.Active = False
                    continue
            
                #empty recieve socket
                if recieve == '':
                    self.Active = False
                    print("Stopping!")
                    continue
                #Pong the server letting them know you're still here
                if recieve == "PING :tmi.twitch.tv\r\n":
                    self.sock.send('PONG :tmi.twitch.tv\r\n'.encode("utf-8"))
                    continue
                try:
                    print(recieve)
                except:
                    print("Unexpected error while recieving")
                    continue
                msg = recieve.split(" ")

                #Give msg to all components
                for comp in self.Components:
                    try:
                        ret = comp.getMsg(msg)
                    except:
                        print("Unexpected error at: " + str(comp))
                        ret = ""
                        
                    if ret != "":
                        self.SendMsg("/me " + ret)
                    
            #Bot Shutdown msg
                if msg[2] == "#"+UserName and msg[3] == ":shutdown\r\n":
                    self.Active = False
                    continue
            else:
                for comp in self.Components:
                    try:
                        ret = comp.update()
                    except:
                        print("Unexpected error at: " + str(comp))
                        ret = ""
                    if ret != "":
                        self.SendMsg("/me " + ret)
            time.sleep(0.1)
        self.Shutdown()
        pass

    def Shutdown(self):
        for comp in self.Components:
            comp.shutdown()
        if self.sock:
            if self.Error:
                self.SendMsg("/me Noobot broke <(x&x)>")
            else:
                self.SendMsg("/me Noobot signing out! Bye Chat <(o8o)^")
            time.sleep(0.1)
            self.sock.send('PART #{}'.format(self.cfg.get("User","UserName")).encode("utf-8"))
            self.sock.close()

twitchbot= TwitchBot()
try:
    twitchbot.Connect()
    twitchbot.Start()
    pass
except KeyboardInterrupt:
    print("Manually shutting down bot")
    twitchbot.Shutdown()
    
